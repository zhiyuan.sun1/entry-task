package constant

import "time"

const (
	SessionExpire        = 15 * time.Minute
	DetailExpire         = 15 * time.Minute
	InfoExpire           = 15 * time.Minute
	SearchExpire         = 15 * time.Minute
	CommentListExpire    = 15 * time.Minute
	SubCommentListExpire = 15 * time.Minute
	CommentContentExpire = 15 * time.Minute
)
