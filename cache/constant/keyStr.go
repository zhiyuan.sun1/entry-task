package constant

const (
	KeySession = "session_cache_%v"

	KeyIdListAll        = "id_list_all"
	KeyIdListByCategory = "id_list_category_%v"
	KeyIdListByTitle    = "id_list_title_%v"

	KeyProductCache     = "product_cache_%v"
	KeyProductInfoCache = "product_info_cache_%v"

	KeyCommentIdList    = "id_list_comment_%v"
	KeySubCommentIdList = "id_list_sub_comment_%v"
	KeyComment          = "comment_cache_%v"
)
