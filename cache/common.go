package cache

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/szy/entry-task/cache/constant"
	"strconv"
	"time"
)

func (c *Cache) GetIdBySession(session string) (int64, error) {
	client := c.cache
	idStr, err := client.Get(fmt.Sprintf(constant.KeySession, session)).Result()
	if err == redis.Nil {
		return 0, nil
	} else if err != nil {
		return 0, err
	}
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (c *Cache) getZList(keyPrefix string, cursor int64, limit int64) ([]string, error) {
	client := c.cache
	return client.ZRange(keyPrefix, cursor, limit).Result()
}

func (c *Cache) cacheZListForId(key string, idList []int64, expire time.Duration, nilId float64) error {
	client := c.cache
	pipeline := client.Pipeline()
	pipeline.ZAdd(key, redis.Z{
		Score:  nilId,
		Member: nilId,
	})
	zset := make([]redis.Z, len(idList))
	for i, id := range idList {
		zset[i] = redis.Z{
			Score:  float64(id),
			Member: id,
		}
	}
	n := 0
	for ; n < len(zset); n += 10000 {
		if n+10000 < len(zset) {
			pipeline.ZAdd(key, zset[n:n+10000]...)
		} else {
			pipeline.ZAdd(key, zset[n:]...)
		}
		_, err := pipeline.Exec()
		if err != nil {
			return err
		}
		pipeline = client.Pipeline()
		fmt.Println(n)
	}
	if expire == 0 {
		pipeline.Persist(key)
	} else {
		pipeline.Expire(key, expire)
	}
	_, err := pipeline.Exec()
	return err
}
