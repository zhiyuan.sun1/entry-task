package cache

import (
	"fmt"
	"github.com/szy/entry-task/cache/constant"
)

func (c *Cache) SetSession(sessionId string, id int64) error {
	client := c.cache
	//fmt.Println("set: ", fmt.Sprintf(cache2.KeySession, sessionId))
	return client.Set(fmt.Sprintf(constant.KeySession, sessionId), id, constant.SessionExpire).Err()
}

func (c *Cache) ExistsSession(sessionId string) (int64, error) {
	client := c.cache
	//fmt.Println("get: ", fmt.Sprintf(cache2.KeySession, sessionId))
	return client.Exists(fmt.Sprintf(constant.KeySession, sessionId)).Result()
}
