package cache

import (
	"github.com/go-redis/redis"
	"github.com/szy/entry-task/global/resource"
)

type Cache struct {
	cache *redis.Client
}

func New() *Cache {
	return &Cache{cache: resource.RedisClient}
}
