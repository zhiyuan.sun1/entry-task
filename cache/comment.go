package cache

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/szy/entry-task/cache/constant"
	"github.com/szy/entry-task/models"
	"github.com/szy/entry-task/utils/log"
	"strconv"
	"time"
)

func (c *Cache) GetCommentIdList(key string, cursor int64, limit int64) ([]string, error) {
	if cursor < constant.FirstCommentId {
		cursor = constant.FirstCommentId
	}
	return c.getZList(key, cursor, limit)
}

func (c *Cache) CacheCommentIdList(key string, idList []int64, expire time.Duration) error {
	return c.cacheZListForId(key, idList, expire, constant.NilCommentId)
}

func (c *Cache) CacheSubCommentIdLists(pairList []models.Pair, parentList []string) error {
	client := c.cache
	pipeline := client.Pipeline()

	for _, p := range pairList {
		pipeline.ZAdd(fmt.Sprintf(constant.KeySubCommentIdList, p.ParentId), redis.Z{
			Score:  float64(p.Id),
			Member: p.Id,
		})
		//fmt.Println("caching sub comment: ", fmt.Sprintf(cache.KeySubCommentIdList, p.ParentId))
	}
	for _, p := range parentList {
		pipeline.ZAdd(fmt.Sprintf(constant.KeySubCommentIdList, p), redis.Z{
			Score:  constant.NilCommentId,
			Member: constant.NilCommentId,
		})
		pipeline.Expire(fmt.Sprintf(constant.KeySubCommentIdList, p), constant.SubCommentListExpire)
	}
	_, err := pipeline.Exec()
	return err
}

func (c *Cache) GetSubCommentIdFromIdList(idList []string) ([]string, []string) {
	client := c.cache
	pipeline := client.Pipeline()
	rtList := make([]string, 0)
	missSet := make(map[string]interface{})
	rangeResultSet := make(map[string]*redis.StringSliceCmd)
	existsResultSet := make(map[string]*redis.IntCmd)
	for _, id := range idList {
		key := fmt.Sprintf(constant.KeySubCommentIdList, id)
		//fmt.Println("getting sub comment: ", key)
		existsResultSet[id] = pipeline.Exists(key)
		rangeResultSet[id] = pipeline.ZRange(key, 1, 10)
		missSet[id] = new(interface{})
	}
	pipeline.Exec()
	//if err == redis.Nil {
	//	fmt.Println("cache missed when get cache from comment id list: ", idList)
	//} else if err != nil {
	//	fmt.Println("err occurred when get cache from comment id list: ", idList)
	//}
	//fmt.Println(rangeResultSet)
	for _, cmd := range rangeResultSet {
		//cmd, ok := cmdRes.(*redis.SliceCmd)
		if cmd.Err() == nil && len(cmd.Val()) != 0 {
			//fmt.Println(cmd.Result())
			result, err := cmd.Result()
			if err == redis.Nil {
				continue
			} else if err != nil {
				if err != nil {
					log.Logln(err)
				}
				continue
			}
			rtList = append(rtList, result...)
		}
	}
	for id, cmd := range existsResultSet {
		if cmd.Err() == nil && cmd.Val() > 0 {
			delete(missSet, id)
		}
	}
	missList := make([]string, len(missSet))
	i := 0
	for k := range missSet {
		missList[i] = k
		i++
	}
	return rtList, missList
}

func (c *Cache) GetCommentListByIdList(idList []string) ([]models.Comment, []string) {
	client := c.cache
	pipeline := client.Pipeline()
	rtList := make([]models.Comment, 0)
	missSet := make(map[string]interface{})
	for _, id := range idList {
		pipeline.Get(fmt.Sprintf(constant.KeyComment, id))
		missSet[id] = new(interface{})
	}
	cmdReturn, _ := pipeline.Exec()
	//if err == redis.Nil {
	//	fmt.Println("cache missed when get cache from comment id list: ", idList)
	//} else if err != nil {
	//	fmt.Println("err occurred when get cache from comment id list: ", idList)
	//}
	for _, cmdRes := range cmdReturn {
		cmd, ok := cmdRes.(*redis.StringCmd)
		if ok {
			result, err := cmd.Result()
			if err == redis.Nil {
				continue
			} else if err != nil {
				if err != nil {
					log.Logln(err)
				}
				continue
			}
			comment := &models.Comment{}
			err = json.Unmarshal([]byte(result), comment)
			if err != nil {
				log.Logln(err)
				continue
			}
			rtList = append(rtList, *comment)
			delete(missSet, strconv.FormatInt(comment.Id, 10))
		}
	}
	missList := make([]string, len(missSet))
	i := 0
	for k := range missSet {
		missList[i] = k
		i++
	}
	return rtList, missList
}

func (c *Cache) CacheCommentList(commentList []models.Comment) error {
	client := c.cache
	pipeline := client.Pipeline()

	for _, comment := range commentList {
		//fmt.Println("caching comment !! ", comment)

		bytes, err := json.Marshal(comment)
		if err != nil {
			return err
		}
		pipeline.Set(fmt.Sprintf(constant.KeyComment, comment.Id), bytes, constant.CommentContentExpire)
	}
	_, err := pipeline.Exec()
	return err
}

func (c *Cache) RemoveCommentIdToList(key string) error {
	client := c.cache
	return client.Del(key).Err()
}
