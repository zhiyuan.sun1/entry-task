package cache

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/szy/entry-task/cache/constant"
	"github.com/szy/entry-task/models"
	"github.com/szy/entry-task/utils/log"
	"strconv"
	"time"
)

func (c *Cache) RemoveProductId() error {
	return c.cache.Del(constant.KeyIdListAll).Err()
}

func (c *Cache) LoadProductId(idList []int64) error {
	return c.CacheSearch(constant.KeyIdListAll, idList, 0)
}

func (c *Cache) CacheProductInfos(infos []*models.ProductInfo) error {
	client := c.cache
	pipeline := client.Pipeline()
	//fmt.Println(" details is !! ", details)
	for _, info := range infos {
		//fmt.Println(client, " detail is !! ", detail)

		bytes, err := json.Marshal(info)
		if err != nil {
			return err
		}
		pipeline.Set(fmt.Sprintf(constant.KeyProductInfoCache, info.Id), bytes, constant.InfoExpire)
	}
	_, err := pipeline.Exec()
	return err
}

func (c *Cache) CacheProducts(details []*models.ProductDetail) error {
	client := c.cache
	pipeline := client.Pipeline()
	//fmt.Println(" details is !! ", details)
	for _, detail := range details {
		//fmt.Println(client, " detail is !! ", detail)

		bytes, err := json.Marshal(detail)
		if err != nil {
			return err
		}
		pipeline.Set(fmt.Sprintf(constant.KeyProductCache, detail.Id), bytes, constant.DetailExpire)
	}
	_, err := pipeline.Exec()
	return err
}

func (c *Cache) ExistsProductInfo(id string) (bool, error) {
	result, err := c.cache.Exists(fmt.Sprintf(constant.KeyProductInfoCache, id)).Result()
	if err != nil {
		return false, err
	}
	return result > 0, nil
}
func (c *Cache) ExistsProductDetail(id string) (bool, error) {
	result, err := c.cache.Exists(fmt.Sprintf(constant.KeyProductCache, id)).Result()
	if err != nil {
		return false, err
	}
	return result > 0, nil
}

func (c *Cache) GetProductInfoFromIdList(idList []string) ([]models.ProductInfo, []string) {
	client := c.cache
	pipeline := client.Pipeline()
	rtList := make([]models.ProductInfo, 0)
	missSet := make(map[string]interface{})
	for _, id := range idList {
		pipeline.Get(fmt.Sprintf(constant.KeyProductInfoCache, id))
		missSet[id] = new(interface{})
	}
	cmdReturn, _ := pipeline.Exec()
	//if err == redis.Nil {
	//	fmt.Println("cache missed when get cache from id list: ", idList)
	//} else if err != nil {
	//	fmt.Println("err occurred when get cache from id list: ", idList)
	//}
	for _, cmdRes := range cmdReturn {
		cmd, ok := cmdRes.(*redis.StringCmd)
		if ok {
			result, err := cmd.Result()
			if len(result) == 0 {
				continue
			}
			if err == redis.Nil {
				continue
			} else if err != nil {
				if err != nil {
					log.Logln(err)
				}
				continue
			}
			info := &models.ProductInfo{}
			err = json.Unmarshal([]byte(result), info)
			if err != nil {
				//fmt.Println("result: ", result, ", err: ", err)
				continue
			}
			rtList = append(rtList, *info)
			delete(missSet, strconv.FormatInt(info.Id, 10))
		}
	}
	missList := make([]string, len(missSet))
	i := 0
	for k := range missSet {
		missList[i] = k
		i++
	}
	return rtList, missList
}

func (c *Cache) GetProductDetail(id string) (*models.ProductDetail, error) {
	client := c.cache
	result, err := client.Get(fmt.Sprintf(constant.KeyProductCache, id)).Result()
	if err == redis.Nil {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	detail := &models.ProductDetail{}
	err = json.Unmarshal([]byte(result), detail)
	if err != nil {
		return nil, err
	}
	return detail, nil
}

func (c *Cache) CacheSearch(key string, idList []int64, expire time.Duration) error {
	return c.cacheZListForId(key, idList, expire, constant.NilProductId)
}

func (c *Cache) ExistsCache(sc string) (bool, error) {
	result, err := c.cache.Exists(sc).Result()
	if err != nil {
		return false, err
	}
	return result > 0, nil
}

func (c *Cache) GetProductIdList(keyPrefix string, cursor int64, limit int64) ([]string, error) {
	if cursor < constant.FirstProductId {
		cursor = constant.FirstProductId
	}
	return c.getZList(keyPrefix, cursor, limit)
}
