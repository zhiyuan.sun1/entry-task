package models

import proto "github.com/szy/entry-task/server/grpc/product"

type ProductDetail struct {
	Id          int64   `json:"id"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	PicUrl      string  `json:"pic_url"`
	Categories  []int32 `gorm:"-"`
}

func (p *ProductDetail) ToProto() *proto.DetailReply {
	return &proto.DetailReply{
		Id:          p.Id,
		Title:       p.Title,
		Description: p.Description,
		//should convert to real picture data, or use some other method
		PicUrl:     p.PicUrl,
		Categories: p.Categories,
	}
}
