package models

import proto "github.com/szy/entry-task/server/grpc/product"

type ProductInfo struct {
	Id     int64  `json:"id"`
	Title  string `json:"title"`
	PicUrl string `json:"pic_url"`
}

func (p *ProductInfo) ToProto() *proto.ProductInfo {
	return &proto.ProductInfo{
		Id:    p.Id,
		Title: p.Title,
		//should convert to real picture data
		Picture: p.PicUrl,
	}
}
