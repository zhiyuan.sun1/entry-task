package models

type Pair struct {
	Id       int64 `json:"id"`
	ParentId int64 `json:"parent_id"`
}
