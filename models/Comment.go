package models

import proto "github.com/szy/entry-task/server/grpc/comment"

type Comment struct {
	Id        int64  `json:"id"`
	Content   string `json:"content"`
	ProductId int64  `json:"product_id"`
	ParentId  int64  `json:"parent_id"`
	UserId    int64  `json:"user_id"`
}

func (c *Comment) ToProto() *proto.CommentInfo {
	return &proto.CommentInfo{
		Id:        c.Id,
		Content:   c.Content,
		ProductId: c.ProductId,
		ParentId:  c.ParentId,
		UserId:    c.UserId,
	}
}
