package models

import (
	proto "github.com/szy/entry-task/server/grpc/user"
	"github.com/szy/entry-task/utils/psw"
)

type User struct {
	Id       int64  `json:"id"`
	LogId    string `json:"log_id"`
	Nickname string `json:"nickname"`
	Password string `json:"password"`
}

func CreateUserToRegister(request *proto.RegisterRequest) User {
	return User{
		LogId:    request.LogId,
		Nickname: request.Nickname,
		Password: psw.HashPassword(request.Password),
	}
}
