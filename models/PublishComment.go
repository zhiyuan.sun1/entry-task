package models

import proto "github.com/szy/entry-task/server/grpc/comment"

type PublishComment struct {
	Content    string `json:"content"`
	ProductId  int64  `json:"product_id"`
	ParentId   int64  `json:"parent_id"`
	UserId     int64  `json:"user_id"`
	CreateTime int64  `json:"create_time"`
	ModifyTime int64  `json:"modify_time"`
}

func CreatePublishComment(request *proto.PublishRequest, id int64) *PublishComment {
	return &PublishComment{
		Content:    request.Content,
		ProductId:  request.ProductId,
		ParentId:   request.ParentId,
		UserId:     id,
		CreateTime: 0,
		ModifyTime: 0,
	}
}
