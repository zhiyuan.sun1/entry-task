package grpc_server

import (
	"context"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"github.com/szy/entry-task/cache"
	"github.com/szy/entry-task/dao"
	"github.com/szy/entry-task/global/inner_error"
	"github.com/szy/entry-task/models"
	proto "github.com/szy/entry-task/server/grpc/user"
	"github.com/szy/entry-task/utils/psw"
	"gorm.io/gorm"
)

type UserServerImpl struct {
	ctx   context.Context
	dao   *dao.Dao
	cache *cache.Cache
	proto.UnimplementedUserServiceServer
}

func NewUserServer(ctx context.Context) *UserServerImpl {
	svc := UserServerImpl{
		ctx:   ctx,
		dao:   dao.New(),
		cache: cache.New(),
	}
	return &svc
}

func (svc UserServerImpl) Login(ctx context.Context, request *proto.LoginRequest) (*proto.LoginReply, error) {
	userLogInfo, err := svc.dao.GetUserByLogId(request.LogId)
	fmt.Println(userLogInfo, "\n", err)
	if err == gorm.ErrRecordNotFound {
		return &proto.LoginReply{
			Session:   "",
			ErrorCode: inner_error.NoSuchUser,
		}, nil
	} else if err != nil {
		return nil, err
	}
	if !psw.CheckPsw(userLogInfo.Password, request.Password) {
		//fmt.Println("wrong password with", userLogInfo.Password, " and ", request.Password)
		return &proto.LoginReply{
			ErrorCode: inner_error.WrongPassword,
		}, nil
	}
	session, err := svc.setSession(userLogInfo.Id)
	if err != nil {
		//fmt.Println("set session err: ", err)
		return nil, err
	}

	return &proto.LoginReply{
		Session: session,
	}, nil
}

func (svc UserServerImpl) Register(ctx context.Context, request *proto.RegisterRequest) (*proto.RegisterReply, error) {
	userLogInfo := models.CreateUserToRegister(request)
	affectedRows, err := svc.dao.InsertUser(userLogInfo)
	if err != nil {
		return nil, err
	}
	if affectedRows == 0 {
		//fmt.Println("log id has existed for: ", request.LogId)
		return &proto.RegisterReply{
			ErrorCode: inner_error.DuplicatedUserId,
		}, nil
	}
	return &proto.RegisterReply{}, nil
}

func (svc UserServerImpl) setSession(id int64) (string, error) {
	sessionID := uuid.NewV4().String()
	return sessionID, svc.cache.SetSession(sessionID, id)
}
