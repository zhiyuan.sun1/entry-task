package grpc_server

import (
	"context"
	"errors"
	"github.com/agiledragon/gomonkey/v2"
	"github.com/go-redis/redis"
	"github.com/szy/entry-task/cache"
	"github.com/szy/entry-task/dao"
	"github.com/szy/entry-task/global/inner_error"
	"github.com/szy/entry-task/global/resource"
	"github.com/szy/entry-task/models"
	proto "github.com/szy/entry-task/server/grpc/user"
	"github.com/szy/entry-task/utils/psw"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"os"
	"reflect"
	"testing"
)

var (
	svc *UserServerImpl

	// Mock stuffs
	id       = 0
	userId   = "test_userid"
	nickname = "test_nickname"
	password = "test_password"
)

//func BenchmarkUserServerImpl_Login(b *testing.B) {
//	dsn := "root:szy000@tcp(127.0.0.1:3306)/entry_task?charset=utf8mb4&parseTime=True&loc=Local"
//	var err error
//	global.DBEngine, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
//	if err != nil {
//		log.Fatalln("err: ", err)
//		os.Exit(1)
//	}
//
//	global.RedisClient = redis.NewClient(&redis.Options{
//		Addr:         "localhost:8001", // redis地址
//		Password:     "",               // redis密码，没有则留空
//		DB:           0,                // 默认数据库，默认是0
//		PoolSize:     4096,             // 连接池最大socket连接数，默认为4倍CPU数， 4 * runtime.NumCPU
//		MinIdleConns: 16,
//	})
//	ctx := context.Background()
//
//	svc := NewUserServer(ctx)
//
//	fmt.Println(svc.dao)
//	fmt.Println(svc.cache)
//
//	for i := 0; i < b.N; i++ {
//		svc.Login(ctx, &proto.LoginRequest{
//			Id:       0,
//			LogId:    "stress_test_19999",
//			Password: "stress_test",
//		})
//	}
//}

func init() {
	dsn := "root:szy000@tcp(127.0.0.1:3306)/entry_task?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	resource.DBEngine, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln("err: ", err)
		os.Exit(1)
	}

	resource.RedisClient = redis.NewClient(&redis.Options{
		Addr:         "localhost:8001", // redis地址
		Password:     "",               // redis密码，没有则留空
		DB:           0,                // 默认数据库，默认是0
		PoolSize:     4096,             // 连接池最大socket连接数，默认为4倍CPU数， 4 * runtime.NumCPU
		MinIdleConns: 16,
	})

}

func TestUserServerImpl_Register(t *testing.T) {
	svc = NewUserServer(context.Background())
	// Input
	request := &proto.RegisterRequest{
		LogId:    userId,
		Nickname: nickname,
		Password: password,
	}

	t.Run("normal register", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.dao), "InsertUser", func(_ *dao.Dao, _ models.User) (int64, error) {
			return 1, nil
		})
		defer patches.Reset()
		_, err := svc.Register(context.Background(), request)
		if err != nil {
			t.Error("TestUserServerImpl_Register got an error: ", err)
		}
	})

	t.Run("has existed register", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.dao), "InsertUser", func(_ *dao.Dao, _ models.User) (int64, error) {
			return 0, nil
		})
		defer patches.Reset()
		reply, err := svc.Register(context.Background(), request)
		if err != nil || reply.ErrorCode != inner_error.DuplicatedUserId {
			t.Error("TestUserServerImpl_Register should got a DuplicatedUserId error code, but it got: ", err, " , and: ", reply)
		}
	})
}

func TestUserServerImpl_Login(t *testing.T) {
	request := &proto.LoginRequest{
		LogId:    userId,
		Password: password,
	}

	t.Run("normal login", func(t *testing.T) {

		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.dao), "GetUserByLogId", func(_ *dao.Dao, _ string) (models.User, error) {
			return models.User{
				Id:       int64(id),
				LogId:    userId,
				Nickname: nickname,
				Password: password,
			}, nil
		})
		defer patches.Reset()
		patches.ApplyFunc(psw.HashPassword, func(_ string) string {
			return password
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "SetSession", func(_ *cache.Cache, _ string, _ int64) error {
			return nil
		})
		_, err := svc.Login(context.Background(), request)
		if err != nil {
			t.Error("TestUserServerImpl_Login got an error: ", err)
		}
		//log.Println("TestUserServerImpl_Login: ", response)
	})

	t.Run("no such user login", func(t *testing.T) {

		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.dao), "GetUserByLogId", func(_ *dao.Dao, _ string) (models.User, error) {
			return models.User{}, gorm.ErrRecordNotFound
		})
		defer patches.Reset()
		patches.ApplyFunc(psw.HashPassword, func(_ string) string {
			return ""
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "SetSession", func(_ *cache.Cache, _ string, _ int64) error {
			return nil
		})
		response, err := svc.Login(context.Background(), request)
		if err != nil || response.ErrorCode != inner_error.NoSuchUser {
			t.Error("TestUserServerImpl_Login should get a no such user error, but get a", err, ", and: ", response)
		}
		//log.Println("TestUserServerImpl_Login: ", response)
	})

	t.Run("wrong password login", func(t *testing.T) {

		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.dao), "GetUserByLogId", func(_ *dao.Dao, _ string) (models.User, error) {
			return models.User{
				Id:       int64(id),
				LogId:    userId,
				Nickname: nickname,
				Password: password,
			}, nil
		})
		defer patches.Reset()
		patches.ApplyFunc(psw.HashPassword, func(_ string) string {
			return ""
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "SetSession", func(_ *cache.Cache, _ string, _ int64) error {
			return nil
		})
		response, err := svc.Login(context.Background(), request)
		if err != nil || response.ErrorCode != inner_error.WrongPassword {
			t.Error("TestUserServerImpl_Login should get a WrongPassword error, but get a", err, ", and: ", response)
		}
		//log.Println("TestUserServerImpl_Login: ", response)
	})

	t.Run("session failed login", func(t *testing.T) {

		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.dao), "GetUserByLogId", func(_ *dao.Dao, _ string) (models.User, error) {
			return models.User{
				Id:       int64(id),
				LogId:    userId,
				Nickname: nickname,
				Password: password,
			}, nil
		})
		defer patches.Reset()
		patches.ApplyFunc(psw.HashPassword, func(_ string) string {
			return password
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "SetSession", func(_ *cache.Cache, _ string, _ int64) error {
			return errors.New("session failed")
		})
		response, err := svc.Login(context.Background(), request)
		if err == nil {
			t.Error("TestUserServerImpl_Login should get a session failed error, but get a response: ", response)
		}
		//log.Println("TestUserServerImpl_Login: ", response)
	})
}
