package grpc_server

import (
	"context"
	"fmt"
	"github.com/szy/entry-task/cache"
	"github.com/szy/entry-task/cache/constant"
	"github.com/szy/entry-task/dao"
	"github.com/szy/entry-task/global/inner_error"
	"github.com/szy/entry-task/models"
	proto "github.com/szy/entry-task/server/grpc/product"
	"github.com/szy/entry-task/utils/convert"
	"github.com/szy/entry-task/utils/log"
	"math"
	"strconv"
)

type ProductServerImpl struct {
	ctx   context.Context
	dao   *dao.Dao
	cache *cache.Cache
	proto.UnimplementedProductServiceServer
}

func NewProductServer(ctx context.Context) *ProductServerImpl {
	svc := ProductServerImpl{
		ctx:   ctx,
		dao:   dao.New(),
		cache: cache.New(),
	}
	return &svc
}

func (svc *ProductServerImpl) Browse(ctx context.Context, request *proto.BrowseRequest) (*proto.BrowseReply, error) {
	productInfoList, err := svc.getProductInfoList(request)
	if err != nil {
		return nil, err
	}
	var protoInfoList []*proto.ProductInfo
	for _, info := range productInfoList {
		protoInfoList = append(protoInfoList, info.ToProto())
	}
	hasMore := int64(len(protoInfoList)) >= request.Limit
	cursor := request.Cursor + int64(len(protoInfoList))
	return &proto.BrowseReply{
		InfoList: protoInfoList,
		HasMore:  hasMore,
		Cursor:   cursor,
	}, nil
}

func (svc *ProductServerImpl) Detail(ctx context.Context, request *proto.DetailRequest) (*proto.DetailReply, error) {
	b, err := svc.checkSession(request.SessionId)
	if err != nil {
		log.Logln("error occurred during session check: ", err)
		return nil, err
	}
	if !b {
		//fmt.Println("session check failed")
		return &proto.DetailReply{
			ErrorCode: inner_error.SessionCheckFail,
		}, nil
	}
	id := strconv.FormatInt(request.Id, 10)
	var detail *models.ProductDetail
	exists, err := svc.cache.ExistsProductDetail(id)
	if err != nil {
		log.Logln("check detail exists fail: ", err)
		return nil, err
	}
	if exists {
		detail, err = svc.cache.GetProductDetail(id)
		if err != nil {
			log.Logln("log detail exists fail: ", err)
			return nil, err
		}
	} else {
		detail, err = svc.dao.GetProductDetail(request.Id)
		if err != nil {
			return nil, err
		}
		details := make([]*models.ProductDetail, 1)
		details[0] = detail
		err := svc.cache.CacheProducts(details)
		if err != nil {
			log.Logln("cache failed by: ", err)
		}
	}
	return &proto.DetailReply{
		Id:          detail.Id,
		Title:       detail.Title,
		Description: detail.Description,
		PicUrl:      detail.PicUrl,
		Categories:  detail.Categories,
	}, nil
}

//! better make sure we can choose to cache the info or the detail in one cache structure
//! do not cache all id for every search, just cache first few id
func (svc *ProductServerImpl) getProductInfoList(request *proto.BrowseRequest) ([]models.ProductInfo, error) {
	var cacheKeyPrefix string
	if request.Title != "" {
		cacheKeyPrefix = fmt.Sprintf(constant.KeyIdListByTitle, request.Title)
	} else if request.Category != 0 {
		cacheKeyPrefix = fmt.Sprintf(constant.KeyIdListByCategory, request.Category)
	} else {
		cacheKeyPrefix = constant.KeyIdListAll
	}

	exists, err := svc.cache.ExistsCache(cacheKeyPrefix)
	if err != nil {
		exists = false
		log.Logln("exists search cache error: ", err)
		//return nil, err
	}
	var idList []string
	if exists {
		var limit int64
		if request.Limit == 0 {
			limit = math.MaxInt64
		} else {
			limit = request.Limit
		}
		idList, err = svc.cache.GetProductIdList(cacheKeyPrefix, request.Cursor, limit)
		//fmt.Println("id list: ", idList)
		if err != nil {
			log.Logln("cache unexpected missing for id list: ", err)
			return nil, err
		}
		//fmt.Println("load from cache: ", idList)
	} else {
		idListInt, err := svc.dao.GetProductIdList(request.Limit, request.Title, request.Category)
		if err != nil {
			log.Logln("db get id list error: ", err)
			return nil, err
		}
		idList = convert.SliceInt64ToStr(idListInt)
		err = svc.cache.CacheSearch(cacheKeyPrefix, idListInt, constant.SearchExpire)
		if err != nil {
			log.Logln("cache search error: ", err)
		}

		//fmt.Println("load from db: ", idList)
	}

	//? if we should suppress the error from GetProductDetailFromIdList? because miss list contains the ones which causes an error
	rtList, missList := svc.cache.GetProductInfoFromIdList(idList)
	//fmt.Println("the return list is: ", rtList)
	//fmt.Println("the miss list is: ", missList)

	//if request.Title != "" {
	//	return svc.dao.GetProductListByTitle(request.Title, request.Cursor, request.Limit)
	//} else if request.Category != 0 {
	//	return svc.dao.GetProductListByCategory(request.Category, request.Cursor, request.Limit)
	//}
	//list, err := svc.dao.GetProductList(request.Cursor, request.Limit)
	if len(missList) == 0 {
		return rtList, err
	}
	//fmt.Println("miss one: ", missList)
	regetList, err := svc.dao.GetProductList(convert.SliceStrToInt64(missList))
	if err != nil {
		return nil, err
	}
	//should cache the result here
	pointerList := make([]*models.ProductInfo, len(regetList))
	for i, _ := range regetList {
		pointerList[i] = &(regetList[i])
	}
	err = svc.cache.CacheProductInfos(pointerList)
	if err != nil {
		log.Logln("cache product info with err: ", err)
	}
	//fmt.Println("get the miss from db: ", regetList)
	rtList = append(rtList, regetList...)
	return rtList, nil
}

func (svc *ProductServerImpl) checkSession(session string) (bool, error) {
	num, err := svc.cache.ExistsSession(session)
	if err != nil {
		return false, err
	}
	return num > 0, nil
}
