package grpc_server

import (
	"context"
	"github.com/agiledragon/gomonkey/v2"
	"github.com/go-redis/redis"
	"github.com/szy/entry-task/cache"
	"github.com/szy/entry-task/dao"
	"github.com/szy/entry-task/global/inner_error"
	"github.com/szy/entry-task/global/resource"
	"github.com/szy/entry-task/models"
	proto "github.com/szy/entry-task/server/grpc/product"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"os"
	"reflect"
	"testing"
	"time"
)

var (
	svc *ProductServerImpl

	// Mock stuffs
	id       = 100
	title    = "test_title"
	category = 1
	cursor   = 1
	limit    = 10
)

func init() {
	dsn := "root:szy000@tcp(127.0.0.1:3306)/entry_task?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	resource.DBEngine, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln("err: ", err)
		os.Exit(1)
	}

	resource.RedisClient = redis.NewClient(&redis.Options{
		Addr:         "localhost:8001", // redis地址
		Password:     "",               // redis密码，没有则留空
		DB:           0,                // 默认数据库，默认是0
		PoolSize:     4096,             // 连接池最大socket连接数，默认为4倍CPU数， 4 * runtime.NumCPU
		MinIdleConns: 16,
	})

}

func TestUserServerImpl_Browse(t *testing.T) {
	svc = NewProductServer(context.Background())

	// Input
	testInfo := models.ProductInfo{
		Id:     0,
		Title:  "test",
		PicUrl: "test",
	}

	t.Run("browse with cache", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsCache", func(_ *cache.Cache, _ string) (bool, error) {
			return true, nil
		})
		defer patches.Reset()
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetProductIdList", func(_ *cache.Cache, _ string, _ int64, _ int64) ([]string, error) {
			return []string{"1", "2", "3"}, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetProductInfoFromIdList", func(_ *cache.Cache, _ []string) ([]models.ProductInfo, []string) {
			return []models.ProductInfo{testInfo, testInfo, testInfo}, nil
		})
		_, err := svc.Browse(context.Background(), &proto.BrowseRequest{
			Cursor:   0,
			Limit:    0,
			Title:    "",
			Category: 0,
		})
		if err != nil {
			t.Error("TestProductServerImpl_Browse got an error: ", err)
		}
		//for _, info := range p.InfoList {
		//	println("info list ", info)
		//}
	})

	t.Run("browse with info cache miss", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsCache", func(_ *cache.Cache, _ string) (bool, error) {
			return true, nil
		})
		defer patches.Reset()
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetProductIdList", func(_ *cache.Cache, _ string, _ int64, _ int64) ([]string, error) {
			return []string{"1", "2", "3"}, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetProductInfoFromIdList", func(_ *cache.Cache, _ []string) ([]models.ProductInfo, []string) {
			return nil, []string{"1", "2", "3"}
		})
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "GetProductList", func(_ *dao.Dao, _ []int64) ([]models.ProductInfo, error) {
			return []models.ProductInfo{testInfo, testInfo, testInfo}, nil
		})
		_, err := svc.Browse(context.Background(), &proto.BrowseRequest{
			Cursor:   0,
			Limit:    0,
			Title:    "",
			Category: 0,
		})
		if err != nil {
			t.Error("TestProductServerImpl_Browse got an error: ", err)
		}
		//for _, info := range p.InfoList {
		//	println("info list ", info)
		//}
	})

	t.Run("search with list and info cache miss", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsCache", func(_ *cache.Cache, _ string) (bool, error) {
			return false, nil
		})
		defer patches.Reset()
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "GetProductIdList", func(_ *dao.Dao, _ int64, _ string, _ int32) ([]int64, error) {
			return []int64{1, 2, 3}, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "CacheSearch", func(_ *cache.Cache, _ string, _ []int64, _ time.Duration) error {
			return nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetProductInfoFromIdList", func(_ *cache.Cache, _ []string) ([]models.ProductInfo, []string) {
			return nil, []string{"1", "2", "3"}
		})
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "GetProductList", func(_ *dao.Dao, _ []int64) ([]models.ProductInfo, error) {
			return []models.ProductInfo{testInfo, testInfo, testInfo}, nil
		})
		_, err := svc.Browse(context.Background(), &proto.BrowseRequest{
			Cursor:   0,
			Limit:    10,
			Title:    title,
			Category: 0,
		})
		if err != nil {
			t.Error("TestProductServerImpl_Browse got an error: ", err)
		}
		//for _, info := range p.InfoList {
		//	println("info list ", info)
		//}
	})
}

func TestUserServerImpl_Detail(t *testing.T) {
	svc = NewProductServer(context.Background())

	// Input
	testDetail := models.ProductDetail{
		Id:          0,
		Title:       "test",
		Description: "test",
		PicUrl:      "test",
		Categories:  []int32{1, 2, 3},
	}

	t.Run("detail with cache", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsSession", func(_ *cache.Cache, _ string) (int64, error) {
			return 1, nil
		})
		defer patches.Reset()
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsProductDetail", func(_ *cache.Cache, _ string) (bool, error) {
			return true, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetProductDetail", func(_ *cache.Cache, _ string) (*models.ProductDetail, error) {
			return &testDetail, nil
		})
		_, err := svc.Detail(context.Background(), &proto.DetailRequest{
			Id:        0,
			SessionId: "test_session",
		})
		if err != nil {
			t.Error("TestProductServerImpl_Detail got an error: ", err)
		}
		//for _, info := range p.InfoList {
		//	println("info list ", info)
		//}
	})

	t.Run("detail without cache", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsSession", func(_ *cache.Cache, _ string) (int64, error) {
			return 1, nil
		})
		defer patches.Reset()
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsProductInfo", func(_ *cache.Cache, _ string) (bool, error) {
			return false, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "GetProductDetail", func(_ *dao.Dao, _ int64) (*models.ProductDetail, error) {
			return &testDetail, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "CacheProducts", func(_ *cache.Cache, _ []*models.ProductDetail) error {
			return nil
		})
		_, err := svc.Detail(context.Background(), &proto.DetailRequest{
			Id:        0,
			SessionId: "test_session",
		})
		if err != nil {
			t.Error("TestProductServerImpl_Detail got an error: ", err)
		}
		//for _, info := range p.InfoList {
		//	println("info list ", info)
		//}
	})

	t.Run("detail without session", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsSession", func(_ *cache.Cache, _ string) (int64, error) {
			return 0, nil
		})
		defer patches.Reset()
		reply, err := svc.Detail(context.Background(), &proto.DetailRequest{
			Id:        0,
			SessionId: "test_session",
		})
		if err != nil || reply.ErrorCode != inner_error.SessionCheckFail {
			t.Error("TestProductServerImpl_Detail got an error: ", err, " or it get a wrong inner error code: ", reply.ErrorCode)
		}
		//fmt.Println(reply.ErrorCode)
	})
}
