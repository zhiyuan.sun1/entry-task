package grpc_server

import (
	"context"
	"github.com/agiledragon/gomonkey/v2"
	"github.com/go-redis/redis"
	"github.com/szy/entry-task/cache"
	"github.com/szy/entry-task/dao"
	"github.com/szy/entry-task/global/resource"
	"github.com/szy/entry-task/models"
	proto "github.com/szy/entry-task/server/grpc/comment"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"os"
	"reflect"
	"testing"
	"time"
)

var (
	svc *CommentServerImpl
)

func init() {
	dsn := "root:szy000@tcp(127.0.0.1:3306)/entry_task?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	resource.DBEngine, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln("err: ", err)
		os.Exit(1)
	}

	resource.RedisClient = redis.NewClient(&redis.Options{
		Addr:         "localhost:8001", // redis地址
		Password:     "",               // redis密码，没有则留空
		DB:           0,                // 默认数据库，默认是0
		PoolSize:     4096,             // 连接池最大socket连接数，默认为4倍CPU数， 4 * runtime.NumCPU
		MinIdleConns: 16,
	})

}

func TestCommentServerImpl_Publish(t *testing.T) {
	svc = NewCommentServer(context.Background())
	t.Run("publish", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsSession", func(_ *cache.Cache, _ string) (int64, error) {
			return 1, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "RemoveCommentIdToList", func(_ *cache.Cache, _ string) error {
			return nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "InsertComment", func(_ *dao.Dao, _ *models.PublishComment) error {
			return nil
		})
		_, err := svc.Publish(svc.ctx, &proto.PublishRequest{
			UserId:    0,
			ProductId: 0,
			ParentId:  0,
			Content:   "",
			SessionId: "",
		})
		if err != nil {
			t.Error("TestCommentServerImpl_Publish got an error: ", err)

		}
		_, err = svc.Publish(svc.ctx, &proto.PublishRequest{
			UserId:    0,
			ProductId: 0,
			ParentId:  100,
			Content:   "",
			SessionId: "",
		})
		if err != nil {
			t.Error("TestCommentServerImpl_Publish got an error: ", err)

		}
	})
}

func TestCommentServerImpl_Comment(t *testing.T) {
	svc = NewCommentServer(context.Background())

	commentInfo := models.Comment{
		Id:        0,
		Content:   "test",
		ProductId: 0,
		ParentId:  0,
		UserId:    0,
	}

	t.Run("comment with cache", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsSession", func(_ *cache.Cache, _ string) (int64, error) {
			return 1, nil
		})
		defer patches.Reset()
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsCache", func(_ *cache.Cache, _ string) (bool, error) {
			return true, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetCommentIdList", func(_ *cache.Cache, _ string, _ int64, _ int64) ([]string, error) {
			return []string{"1", "2", "3"}, nil
		})

		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetSubCommentIdFromIdList", func(_ *cache.Cache, _ []string) ([]string, []string) {
			return []string{"4"}, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "GetSubCommentIdFromIdList", func(_ *dao.Dao, _ []int64) ([]models.Pair, error) {
			return []models.Pair{models.Pair{
				Id:       1,
				ParentId: 2,
			}}, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "CacheSubCommentIdLists", func(_ *cache.Cache, _ []models.Pair, _ []string) error {
			return nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetCommentListByIdList", func(_ *cache.Cache, _ []string) (_ []models.Comment, _ []string) {
			return []models.Comment{commentInfo}, nil
		})
		_, err := svc.Comment(context.Background(), &proto.CommentRequest{
			ProductId: 0,
			SessionId: "test",
		})
		if err != nil {
			t.Error("TestCommentServerImpl_Comment got an error: ", err)
		}
	})

	t.Run("comment with no cache", func(t *testing.T) {
		patches := gomonkey.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsSession", func(_ *cache.Cache, _ string) (int64, error) {
			return 1, nil
		})
		defer patches.Reset()
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "ExistsCache", func(_ *cache.Cache, _ string) (bool, error) {
			return false, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetCommentIdList", func(_ *cache.Cache, _ string, _ int64, _ int64) ([]string, error) {
			return nil, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "GetCommentIdList", func(_ *dao.Dao, _ int64, _ int64, _ int64) ([]int64, error) {
			return []int64{1, 2, 3}, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "CacheCommentIdList", func(_ *cache.Cache, _ string, _ []int64, _ time.Duration) error {
			return nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetSubCommentIdFromIdList", func(_ *cache.Cache, _ []string) ([]string, []string) {
			return nil, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "GetSubCommentIdFromIdList", func(_ *dao.Dao, _ []int64) ([]models.Pair, error) {
			return []models.Pair{models.Pair{
				Id:       1,
				ParentId: 2,
			}}, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "CacheSubCommentIdLists", func(_ *cache.Cache, _ []models.Pair, _ []string) error {
			return nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "GetCommentListByIdList", func(_ *cache.Cache, _ []string) (_ []models.Comment, _ []string) {
			return nil, []string{"1", "2"}
		})
		patches.ApplyMethod(reflect.TypeOf(svc.dao), "GetCommentList", func(_ *dao.Dao, _ []int64) ([]models.Comment, error) {
			return []models.Comment{models.Comment{
				Id:        0,
				Content:   "test",
				ProductId: 0,
				ParentId:  0,
				UserId:    0,
			}}, nil
		})
		patches.ApplyMethod(reflect.TypeOf(svc.cache), "CacheCommentList", func(_ *cache.Cache, _ []models.Comment) error {
			return nil
		})
		_, err := svc.Comment(context.Background(), &proto.CommentRequest{
			ProductId: 0,
			SessionId: "test",
		})
		if err != nil {
			t.Error("TestCommentServerImpl_Comment got an error: ", err)
		}
	})

}
