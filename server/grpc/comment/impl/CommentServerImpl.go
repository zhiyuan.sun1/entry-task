package grpc_server

import (
	"context"
	"fmt"
	"github.com/szy/entry-task/cache"
	"github.com/szy/entry-task/cache/constant"
	"github.com/szy/entry-task/dao"
	"github.com/szy/entry-task/global/inner_error"
	"github.com/szy/entry-task/models"
	proto "github.com/szy/entry-task/server/grpc/comment"
	"github.com/szy/entry-task/utils/convert"
	"github.com/szy/entry-task/utils/log"
	"math"
	"strconv"
)

type CommentServerImpl struct {
	ctx   context.Context
	dao   *dao.Dao
	cache *cache.Cache
	proto.UnimplementedCommentServiceServer
}

func NewCommentServer(ctx context.Context) *CommentServerImpl {
	svc := CommentServerImpl{
		ctx:   ctx,
		dao:   dao.New(),
		cache: cache.New(),
	}
	return &svc
}

func (svc *CommentServerImpl) Comment(ctx context.Context, request *proto.CommentRequest) (*proto.CommentReply, error) {
	b, err := svc.checkSession(request.SessionId)
	//fmt.Println("comment: ", b, " err: ", err)
	if !b {
		log.Logln("session check failed: \"", err == nil, "\"")
		return &proto.CommentReply{
			ErrorCode: inner_error.SessionCheckFail,
		}, nil
	}
	idList, err := svc.getCommentIdList(request.ProductId)
	commentList, err := svc.getCommentList(idList)
	//commentList, err := svc.dao.GetComments(request.ProductId)
	if err != nil {
		return nil, err
	}
	var protoCommentList []*proto.CommentInfo
	for _, comment := range commentList {
		protoCommentList = append(protoCommentList, comment.ToProto())
	}
	return &proto.CommentReply{
		Comments: protoCommentList,
	}, nil
}

func (svc *CommentServerImpl) Publish(ctx context.Context, request *proto.PublishRequest) (*proto.PublishReply, error) {
	id, err := svc.checkSessionAndGetId(request.SessionId)
	if err != nil {
		return nil, err
	} else if id == 0 {
		return &proto.PublishReply{
			ErrorCode: inner_error.SessionCheckFail,
		}, nil
	}
	comment := models.CreatePublishComment(request, id)
	err = svc.dao.InsertComment(comment)
	if err != nil {
		return nil, err
	}
	err = svc.expirePublishedCommentIdList(comment)
	if err != nil {
		return nil, err
	}
	return &proto.PublishReply{}, nil
}

func (svc *CommentServerImpl) getCommentList(idList []string) ([]models.Comment, error) {
	commentList, missList := svc.cache.GetCommentListByIdList(idList)
	//fmt.Println("the return cached comment list is: ", commentList)
	//fmt.Println("the miss comment list is: ", missList)
	if len(missList) == 0 {
		return commentList, nil
	}
	regetList, err := svc.dao.GetCommentList(convert.SliceStrToInt64(missList))
	if err != nil {
		log.Logln("get comments from db error: ", err)
		return nil, err
	}
	//fmt.Println("get the miss comments from db: ", regetList)
	err = svc.cache.CacheCommentList(regetList)
	if err != nil {
		log.Logln("caching comments err: ", err)
	}
	commentList = append(commentList, regetList...)
	return commentList, nil
}

func (svc *CommentServerImpl) getCommentIdList(productId int64) ([]string, error) {
	var cacheKey string
	cacheKey = fmt.Sprintf(constant.KeyCommentIdList, productId)
	exists, err := svc.cache.ExistsCache(cacheKey)
	if err != nil {
		log.Logln("exists comment cache error: ", err)
		exists = false
	}
	var idList []string
	var limit int64 = math.MaxInt64
	var subLimit int64 = math.MaxInt64

	if exists {
		idList, err = svc.cache.GetCommentIdList(cacheKey, 1, limit)
		if err != nil {
			log.Logln("cache unexpected missing for id list: ", err)
			return nil, err
		}
		//fmt.Println("load comment from cache: ", idList)
	} else {
		idListInt, err := svc.dao.GetCommentIdList(productId, 0, limit)
		if err != nil {
			log.Logln("db get comment id list error: ", err)
			return nil, err
		}
		idList = convert.SliceInt64ToStr(idListInt)
		err = svc.cache.CacheCommentIdList(cacheKey, idListInt, constant.CommentListExpire)
		if err != nil {
			log.Logln("cache comment id list error: ", err)
		}
		//fmt.Println("load comment from db: ", idList)
	}
	subList, err := svc.getSubCommentIdList(idList, 0, subLimit)
	//fmt.Println("sub comment list got as: ", subList)
	if err != nil {
		log.Logln("sub comment id get error: ", err)
		return nil, err
	}
	idList = append(idList, subList...)
	return idList, err
}

func (svc *CommentServerImpl) getSubCommentIdList(idList []string, cursor int64, limit int64) ([]string, error) {
	var subList []string
	subList, missList := svc.cache.GetSubCommentIdFromIdList(idList)
	//fmt.Println("the return sub list is: ", subList)
	//fmt.Println("the miss sub list is: ", missList)
	if len(missList) == 0 {
		return subList, nil
	}
	regetPairList, err := svc.dao.GetSubCommentIdFromIdList(convert.SliceStrToInt64(missList))
	if err != nil {
		return nil, err
	}
	//fmt.Println("get the miss sub comment list from db: ", regetPairList)
	err = svc.cache.CacheSubCommentIdLists(regetPairList, idList)
	if err != nil {
		log.Logln("cache sub comment id list err: ", err)
	}
	regetSubIdList := make([]string, len(regetPairList))
	for i, p := range regetPairList {
		regetSubIdList[i] = strconv.FormatInt(p.Id, 10)
	}
	subList = append(subList, regetSubIdList...)
	return subList, nil
}

func (svc *CommentServerImpl) checkSession(session string) (bool, error) {
	num, err := svc.cache.ExistsSession(session)
	if err != nil {
		return false, err
	}
	return num > 0, nil
}

func (svc *CommentServerImpl) checkSessionAndGetId(session string) (int64, error) {
	return svc.cache.GetIdBySession(session)
}

func (svc *CommentServerImpl) expirePublishedCommentIdList(comment *models.PublishComment) error {
	var key string
	if comment.ParentId == 0 {
		key = fmt.Sprintf(constant.KeyCommentIdList, comment.ProductId)
	} else {
		key = fmt.Sprintf(constant.KeySubCommentIdList, comment.ParentId)
	}
	return svc.cache.RemoveCommentIdToList(key)
}
