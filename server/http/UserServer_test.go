package http_server

import (
	"context"
	"github.com/szy/entry-task/global/resource"
	"google.golang.org/grpc"
	"log"
	"testing"
)

func BenchmarkBaseServer_Login(b *testing.B) {
	ctx := context.Background()
	client, err := grpc.DialContext(ctx, ":8082", grpc.WithInsecure())
	if err != nil {
		log.Println("dial err: ", err)
	}
	resource.GRPCClient = client
	resource.InitGRPCClient()
	bs := New(ctx)

	for i := 0; i < b.N; i++ {
		bs.Login(&LoginRequest{
			LogId:    "stress_test_19999",
			Password: "stress_test",
		})
	}
}
