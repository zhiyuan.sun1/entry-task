package http_server

import (
	"github.com/szy/entry-task/global/resource"
	proto "github.com/szy/entry-task/server/grpc/comment"
	"github.com/szy/entry-task/utils/log"
)

type CommentRequest struct {
	ProductId int64  `form:"product_id"`
	SessionId string `form:"session_id"`
}

type CommentResponse struct {
	CommentList []*proto.CommentInfo `json:"comment_list"`
	ErrorCode   int64                `form:"error_code"`
}

type PublishRequest struct {
	ProductId int64  `form:"product_id" binding:"required"`
	ParentId  int64  `form:"parent_id"`
	Content   string `form:"content" binding:"required"`
	SessionId string `form:"session_id"`
}

type PublishResponse struct {
	ErrorCode int64
}

func (server *BaseServer) Comment(param *CommentRequest) (*CommentResponse, error) {
	resp, err := server.getCommentServerClient().Comment(server.ctx, &proto.CommentRequest{
		ProductId: param.ProductId,
		SessionId: param.SessionId,
	})
	if err != nil {
		log.Logln("comment err from r: ", err)
		return nil, err
	}
	return createCommentResponse(resp), nil
}

func (server *BaseServer) Publish(param *PublishRequest) (*PublishResponse, error) {
	resp, err := server.getCommentServerClient().Publish(server.ctx, &proto.PublishRequest{
		SessionId: param.SessionId,
		ProductId: param.ProductId,
		ParentId:  param.ParentId,
		Content:   param.Content,
	})
	if err != nil {
		log.Logln("publish err from r: ", err)
		return nil, err
	}
	return createPublishResponse(resp), nil

}

func (server *BaseServer) getCommentServerClient() proto.CommentServiceClient {
	return resource.GetCommentClient()
}

func createCommentResponse(r *proto.CommentReply) *CommentResponse {
	//fmt.Println("error code: ", r.ErrorCode)
	return &CommentResponse{
		CommentList: r.Comments,
		ErrorCode:   r.ErrorCode,
	}
}

func createPublishResponse(r *proto.PublishReply) *PublishResponse {
	return &PublishResponse{
		ErrorCode: r.ErrorCode,
	}
}
