package http_server

import (
	"context"
	"github.com/szy/entry-task/global/resource"
	"google.golang.org/grpc"
)

type BaseServer struct {
	ctx    context.Context
	client *grpc.ClientConn
}

func New(ctx context.Context) *BaseServer {
	server := &BaseServer{ctx: ctx}
	server.client = resource.GRPCClient
	return server
}

func (server *BaseServer) Close() {
	//if server.client != nil {
	//	err := server.client.Close()
	//	if err != nil {
	//		log.Fatalln("close err: ", err)
	//		return
	//	}
	//}
}
