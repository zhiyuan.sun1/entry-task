package http_server

import (
	"github.com/szy/entry-task/global/resource"
	proto "github.com/szy/entry-task/server/grpc/user"
	"github.com/szy/entry-task/utils/log"
)

type LoginRequest struct {
	LogId    string `form:"log_id" binding:"required,min=6,max=64"`
	Password string `form:"password" binding:"required,min=6,max=64"`
}

type LoginResponse struct {
	SessionId string `form:"session_id"`
	ErrorCode int64  `form:"error_code"`
}

type RegisterRequest struct {
	LogId    string `form:"log_id" binding:"required,min=6,max=64"`
	Password string `form:"password" binding:"required,min=6,max=64"`
	Nickname string `form:"nickname" binding:"required,min=1,max=64"`
}

type RegisterResponse struct {
	ErrorCode int64 `form:"error_code"`
}

func (server *BaseServer) Login(param *LoginRequest) (*LoginResponse, error) {
	resp, err := server.getUserServerClient().Login(server.ctx, &proto.LoginRequest{
		LogId:    param.LogId,
		Password: param.Password,
	})
	if err != nil {
		log.Logln("login err from r: ", err)
		return nil, err
	}
	return &LoginResponse{
		ErrorCode: resp.ErrorCode,
		SessionId: resp.Session,
	}, nil
}

func (server *BaseServer) Register(param *RegisterRequest) (*RegisterResponse, error) {
	resp, err := server.getUserServerClient().Register(server.ctx, &proto.RegisterRequest{
		LogId:    param.LogId,
		Nickname: param.Nickname,
		Password: param.Password,
	})
	if err != nil {
		log.Logln(err)
		return nil, err
	}
	return &RegisterResponse{
		ErrorCode: resp.ErrorCode,
	}, err
}

func (server *BaseServer) getUserServerClient() proto.UserServiceClient {
	return resource.GetUserClient()
}
