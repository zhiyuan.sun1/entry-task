package http_server

import (
	"github.com/szy/entry-task/global/resource"
	proto "github.com/szy/entry-task/server/grpc/product"
	"github.com/szy/entry-task/utils/log"
)

type BrowseRequest struct {
	Cursor   int64  `form:"cursor"`
	Limit    int64  `form:"limit"`
	Title    string `form:"title"`
	Category int32  `form:"category"`
}

type BrowseResponse struct {
	InfoList  []*proto.ProductInfo `json:"list"`
	HasMore   bool                 `json:"has_more"`
	Cursor    int64                `json:"cursor"`
	ErrorCode int64                `form:"error_code"`
}

type DetailRequest struct {
	Id        int64  `form:"id"`
	SessionId string `form:"session_id"`
}

type DetailResponse struct {
	Id          int64   `form:"id"`
	Title       string  `form:"title"`
	Description string  `form:"description"`
	Picture     string  `form:"picture"`
	Categories  []int32 `form:"categories"`
	ErrorCode   int64   `form:"error_code"`
}

func (server *BaseServer) Browse(param *BrowseRequest) (*BrowseResponse, error) {
	resp, err := server.getProductServerClient().Browse(server.ctx, &proto.BrowseRequest{
		Cursor:   param.Cursor,
		Limit:    param.Limit,
		Title:    param.Title,
		Category: param.Category,
	})
	if err != nil {
		log.Logln("browse err from r: ", err)
		return nil, err
	}
	return &BrowseResponse{
		InfoList: resp.InfoList,
		HasMore:  resp.HasMore,
		Cursor:   resp.Cursor,
	}, nil
}

func (server *BaseServer) Detail(param *DetailRequest) (*DetailResponse, error) {
	resp, err := server.getProductServerClient().Detail(server.ctx, &proto.DetailRequest{
		Id:        param.Id,
		SessionId: param.SessionId,
	})
	if err != nil {
		log.Logln("detail err from r: ", err)
		return nil, err
	}
	return createDetailResponse(resp), nil
}

func (server *BaseServer) getProductServerClient() proto.ProductServiceClient {
	return resource.GetProductClient()
}

func createDetailResponse(r *proto.DetailReply) *DetailResponse {
	return &DetailResponse{
		Id:          r.Id,
		Title:       r.Title,
		Description: r.Description,
		Picture:     r.PicUrl,
		Categories:  r.Categories,
		ErrorCode:   r.ErrorCode,
	}
}
