package main

import (
	"context"
	"fmt"
	"github.com/spf13/viper"
	"github.com/szy/entry-task/global"
	"github.com/szy/entry-task/global/resource"
	"github.com/szy/entry-task/routers"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	initHttpConfig()

	r := routers.NewRouter()

	s := &http.Server{
		Addr:    fmt.Sprintf(":%v", global.GetHttpPort()),
		Handler: r,
	}

	if setupGrpc() != nil {
		return
	}
	defer resource.GRPCClient.Close()

	go func() {
		err := s.ListenAndServe()
		if err != nil {
			log.Fatalln(err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
}

func initHttpConfig() {
	path, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	viper.AddConfigPath(path)
	viper.SetConfigName("http_config")
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	global.SetHttpServerConfig()

}

func setupGrpc() error {
	ctx := context.Background()
	client, err := grpc.DialContext(ctx, fmt.Sprintf(":%v", global.GetRpcPort()), grpc.WithInsecure())
	if err != nil {
		log.Println("dial err: ", err)
		return err
	}
	resource.GRPCClient = client
	resource.InitGRPCClient()
	return nil
}
