package main

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/go-redis/redis"
	grpcmiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/spf13/viper"
	"github.com/szy/entry-task/cache"
	"github.com/szy/entry-task/dao"
	"github.com/szy/entry-task/global"
	"github.com/szy/entry-task/global/resource"
	commentproto "github.com/szy/entry-task/server/grpc/comment"
	commentserver "github.com/szy/entry-task/server/grpc/comment/impl"
	productproto "github.com/szy/entry-task/server/grpc/product"
	productserver "github.com/szy/entry-task/server/grpc/product/impl"
	userproto "github.com/szy/entry-task/server/grpc/user"
	userserver "github.com/szy/entry-task/server/grpc/user/impl"
	"github.com/szy/entry-task/utils/recover"
	"google.golang.org/grpc"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"net"
	"os"
	"time"
)

func main() {
	initRpcConfig()

	initDB()

	initCache()

	loadCache()

	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpcmiddleware.ChainUnaryServer(
			recover.Recovery,
		)),
	}
	s := grpc.NewServer(opts...)
	c := context.Background()

	userproto.RegisterUserServiceServer(s, userserver.NewUserServer(c))
	productproto.RegisterProductServiceServer(s, productserver.NewProductServer(c))
	commentproto.RegisterCommentServiceServer(s, commentserver.NewCommentServer(c))

	lis, err := net.Listen("tcp", fmt.Sprintf(":%v", global.GetRpcPort()))
	if err != nil {
		log.Fatalln("listen err: ", err)
	}
	err = s.Serve(lis)
	if err != nil {
		log.Fatalln("serve err: ", err)
	}

}

func initRpcConfig() {
	path, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	viper.AddConfigPath(path)
	viper.SetConfigName("rpc_config")
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	global.SetRpcServerConfig()

}

func initDB() {

	//dsn := "root:szy000@tcp(127.0.0.1:3306)/entry_task?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := global.FormDbDsn()
	var err error
	connPool, err := sql.Open(global.GetDbDriver(), dsn)
	connPool.SetMaxOpenConns(20)
	connPool.SetMaxIdleConns(20)
	connPool.SetConnMaxIdleTime(time.Hour)
	connPool.SetConnMaxLifetime(time.Hour)
	if err != nil {
		log.Fatalln(err)
	}
	resource.DBEngine, err = gorm.Open(mysql.New(mysql.Config{Conn: connPool}))

	//global.DBEngine, err = gorm.Open( mysql.Open(dsn), &gorm.Config{ConnPool: connPool})
	if err != nil {
		log.Fatalln("err: ", err)
	}

	//db, err := global.DBEngine.DB()
	//if err != nil {
	//	log.Fatalln("err: ", err)
	//	os.Exit(1)
	//}
	//db.SetMaxIdleConns(10)
	//db.SetMaxOpenConns(300)
	//db.SetConnMaxIdleTime(time.Hour)
	resource.DBEngine.Logger = logger.Default.LogMode(logger.Silent)

	//gorm.DB{}
}

func initCache() {
	//resource.RedisClient = redis.NewClient(&redis.Options{
	//	Addr:         "localhost:8001", // redis地址
	//	Password:     "",               // redis密码，没有则留空
	//	DB:           0,                // 默认数据库，默认是0
	//	PoolSize:     500,              // 连接池最大socket连接数，默认为4倍CPU数， 4 * runtime.NumCPU
	//	MinIdleConns: 16,
	//})

	resource.RedisClient = redis.NewClient(global.FromRedisOption())

	//通过 *redis.Client.Ping() 来检查是否成功连接到了redis服务器
	_, err := resource.RedisClient.Ping().Result()
	if err != nil {
		log.Fatalln("err: ", err)
	}
	resource.RedisClient.FlushDB()
}

func loadCache() {
	db := dao.New()
	//only use 1000w data, because I have no sever  ~(0.0)~
	idList, err := db.GetProductIdList(10000000, "", 0)
	if err != nil {
		log.Fatalln("get product id list err: ", err)
	}
	c := cache.New()

	fmt.Println(len(idList))
	err = c.LoadProductId(idList)
	if err != nil {
		log.Fatalln("cache, load product id err: ", err)
	}
}
