`
better to separate user_id and id, id should be a auto increment row,
generally we use id to identify user, because user_id may be changed and it's bigger than id
`
create table `users` (
    `id` bigint auto_increment not null,
    `log_id`   varchar(64) not null,
    `nickname` varchar(64) not null,
    `password` varchar(32) not null,
    `create_time` int unsigned not null,
    `modify_time` int unsigned not null,
    primary key (`id`),
    unique key (`log_id`)
) engine=innodb default charset=utf8;

insert into users (id,log_id,nickname,password,create_time,modify_time) value (10000,"base_user","base_user","base_user_psw",0,0);

insert into users (id,log_id,nickname,password) value (10000,"base_user","base_user","base_user_psw");


create table `products` (
    `id` bigint auto_increment not null,
    `title` varchar(128) not null,
    `description` text,
    `pic_url` varchar(256),
    `create_time` int unsigned not null,
    `modify_time` int unsigned not null,
    primary key (`id`)
) engine=innodb default charset=utf8;
create index `title_index` on `products` (`title`);

insert into `products` (`title`,`description`,`pic_url`,create_time,modify_time) value ("test_3","its for test3", "test pic",0,0);

create table `category_relations` (
    product_id bigint not null,
    category bigint not null,
    primary key (`product_id`,`category`),
    foreign key (`product_id`) references products(id)
) engine=innodb default charset=utf8;
create index `category_index` on `category_relations` (`category`);

insert into `category_relations` (`product_id`, `category`) value (1,1);

select * from products inner join category_relations on category_relations.product_id=products.id limit 1,4;

create table `comments` (
    `id` bigint auto_increment not null,
    `content` varchar(2048) not null,
    `product_id` bigint not null,
    `parent_id` bigint,
    `user_id` bigint not null,
    `create_time` int unsigned not null,
    `modify_time` int unsigned not null,
    primary key (`id`),
    foreign key (`product_id`) references products(`id`),
    foreign key (`parent_id`) references comments(`id`),
    foreign key (`user_id`) references users(`id`)
) engine=innodb default charset=utf8;

insert into comments (content,product_id,user_id,create_time,modify_time) value ("test1 comment",1,10000,0,0);

alter table `comments` add constraint foreign key(`user_id`) REFERENCES users(`id`);

drop table comments;
drop table category_relations;
drop table products;
drop table users;

SELECT
    table_schema as '数据库',
        sum(table_rows) as '记录数',
        sum(truncate(data_length/1024/1024, 2)) as '数据容量(MB)',
        sum(truncate(index_length/1024/1024, 2)) as '索引容量(MB)',
        sum(truncate(DATA_FREE/1024/1024, 2)) as '碎片占用(MB)'
from information_schema.tables
group by table_schema
order by sum(data_length) desc, sum(index_length) desc;

SELECT
    table_schema as '数据库',
        table_name as '表名',
        table_rows as '记录数',
        truncate(data_length/1024/1024, 2) as '数据容量(MB)',
        truncate(index_length/1024/1024, 2) as '索引容量(MB)',
        truncate(DATA_FREE/1024/1024, 2) as '碎片占用(MB)'
from
    information_schema.tables
where
        table_schema='entry_task'
order by
    data_length desc, index_length desc;