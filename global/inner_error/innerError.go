package inner_error

var (
	SessionCheckFail int64 = 10000
	DuplicatedUserId int64 = 10001
	WrongPassword    int64 = 10002
	NoSuchUser       int64 = 10003
)
