package resource

import commentproto "github.com/szy/entry-task/server/grpc/comment"
import userproto "github.com/szy/entry-task/server/grpc/user"
import productproto "github.com/szy/entry-task/server/grpc/product"

var (
	commentClient commentproto.CommentServiceClient
	userClient    userproto.UserServiceClient
	productClient productproto.ProductServiceClient
)

func InitGRPCClient() {
	commentClient = commentproto.NewCommentServiceClient(GRPCClient)
	userClient = userproto.NewUserServiceClient(GRPCClient)
	productClient = productproto.NewProductServiceClient(GRPCClient)
}

func GetCommentClient() commentproto.CommentServiceClient {
	return commentClient
}

func GetUserClient() userproto.UserServiceClient {
	return userClient
}

func GetProductClient() productproto.ProductServiceClient {
	return productClient
}
