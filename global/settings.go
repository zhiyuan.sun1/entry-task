package global

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
)

const (
	HashSalt = "default-salt"
)

var (
	httpPort string

	rpcPort string

	dbDriver   string
	dbHost     string
	dbPort     string
	dbUsername string
	dbName     string
	dbPsw      string

	cacheAddr         string
	cachePsw          string
	cacheDb           int
	cachePoolSize     int
	cacheMinIdleConns int
)

func SetHttpServerConfig() {
	httpPort = viper.GetString("http.port")
	rpcPort = viper.GetString("rpc.port")
}

func SetRpcServerConfig() {

	rpcPort = viper.GetString("rpc.port")

	dbDriver = viper.GetString("database.driver")
	dbHost = viper.GetString("database.host")
	dbPort = viper.GetString("database.port")
	dbUsername = viper.GetString("database.username")
	dbName = viper.GetString("database.dbName")
	dbPsw = viper.GetString("database.password")

	cacheAddr = viper.GetString("cache.addr")
	cachePsw = viper.GetString("cache.password")
	cacheDb = viper.GetInt("cache.db")
	cachePoolSize = viper.GetInt("cache.pool_size")
	cacheMinIdleConns = viper.GetInt("cache.min_idle_conns")

}

func GetHttpPort() string {
	return httpPort
}

func GetRpcPort() string {
	return rpcPort
}

func GetDbDriver() string {
	return dbDriver
}

func FormDbDsn() string {
	return fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local",
		dbUsername, dbPsw, dbHost, dbPort, dbName)
}

func FromRedisOption() *redis.Options {
	return &redis.Options{
		Addr:         cacheAddr,
		Password:     cachePsw,
		DB:           cacheDb,
		PoolSize:     cachePoolSize,
		MinIdleConns: cacheMinIdleConns,
	}
}
