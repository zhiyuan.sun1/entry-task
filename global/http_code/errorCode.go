package http_code

import "net/http"

var (
	Success = newError(0, "Success")

	InvalidParams = newError(1, "Invalid Params")

	UserExists    = newError(10000, "user exists")
	UserNotLogin  = newError(10001, "user not login")
	RegisterError = newError(10002, "register error")
	WrongPassword = newError(11000, "wrong password")
	NoSuchUser    = newError(11001, "no such user")
	LoginError    = newError(11002, "login error")

	BrowseError = newError(20001, "browse error")
	DetailError = newError(21001, "detail error")

	CommentError = newError(30001, "comment error")

	//SearchError = newError()
)

type Error struct {
	code    int
	message string
}

func newError(code int, message string) Error {
	return Error{
		code:    code,
		message: message,
	}
}

func (e Error) GetCode() int {
	return e.code
}

func (e Error) GetMessage() string {
	return e.message
}

func (e Error) GetHttpStatus() int {
	switch e {
	case InvalidParams:
		return http.StatusBadRequest
	case Success:
		return http.StatusOK
	default:
		return 555
	}
}
