package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
)

const (
	pageMax = 100
)

func main() {
	//创建一个新文件，写入内容 5 句 “http://c.biancheng.net/golang/”
	fmt.Println("ssssass")
	filePath := "/var/root/go/src/entry-task/browse_urls.txt"
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("文件打开失败", err)
	}
	//及时关闭file句柄
	defer file.Close()

	//写入文件时，使用带缓存的 *Writer
	write := bufio.NewWriter(file)
	for i := 0; i < 1000000; i++ {
		page := rand.Int() % pageMax
		_, err := write.WriteString(fmt.Sprintf("http://localhost:8888/api/product/browse?cursor=%d&limit=%d\n", i, i+page))
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	//Flush将缓存的文件真正写入到文件中
	err = write.Flush()
	if err != nil {
		fmt.Println(err)
		return
	}
}
