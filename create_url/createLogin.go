package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	//创建一个新文件，写入内容 5 句 “http://c.biancheng.net/golang/”
	fmt.Println("ssss")
	filePath := "/var/root/go/src/entry-task/urls.txt"
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("文件打开失败", err)
	}
	//及时关闭file句柄
	defer file.Close()

	//写入文件时，使用带缓存的 *Writer
	write := bufio.NewWriter(file)
	for i := 10004; i < 1010000; i++ {
		_, err := write.WriteString(fmt.Sprint("http://localhost:8888/api/user/login POST log_id=stress_test_", i, "&password=stress_test\n"))
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	//Flush将缓存的文件真正写入到文件中
	err = write.Flush()
	if err != nil {
		fmt.Println(err)
		return
	}
}
