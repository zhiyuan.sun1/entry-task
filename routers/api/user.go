package api

import (
	"github.com/gin-gonic/gin"
	"github.com/szy/entry-task/global/http_code"
	"github.com/szy/entry-task/global/inner_error"
	http_server "github.com/szy/entry-task/server/http"
	"github.com/szy/entry-task/utils/log"
	"github.com/szy/entry-task/utils/response"
)

type User struct{}

func NewUser() User {
	return User{}
}

func (_ *User) Login(c *gin.Context) {
	param := http_server.LoginRequest{}
	err := c.ShouldBind(&param)
	if err != nil {
		response.ToErrorResponse(http_code.InvalidParams, c)
		return
	}
	svc := http_server.New(c.Request.Context())
	defer svc.Close()
	loginResp, err := svc.Login(&param)
	if err != nil {
		response.ToErrorResponse(http_code.LoginError, c)
		return
	}
	if loginResp.ErrorCode == inner_error.WrongPassword {
		response.ToErrorResponse(http_code.WrongPassword, c)
		return
	} else if loginResp.ErrorCode == inner_error.NoSuchUser {
		response.ToErrorResponse(http_code.NoSuchUser, c)
		return
	}
	c.SetCookie("session_id", loginResp.SessionId, 3600, "/", "", false, true)
	response.ToResponse(loginResp, c)
}

func (_ *User) Register(c *gin.Context) {
	param := http_server.RegisterRequest{}
	err := c.ShouldBind(&param)
	if err != nil {
		log.Logln(err)
		response.ToErrorResponse(http_code.InvalidParams, c)
		return
	}
	svc := http_server.New(c.Request.Context())
	defer svc.Close()
	registerResp, err := svc.Register(&param)

	if err != nil {
		response.ToErrorResponse(http_code.RegisterError, c)
		return
	}
	if registerResp.ErrorCode == inner_error.DuplicatedUserId {
		response.ToErrorResponse(http_code.UserExists, c)
		return
	}
	response.ToResponse(registerResp, c)
}
