package api

import (
	"github.com/gin-gonic/gin"
	"github.com/szy/entry-task/global/http_code"
	"github.com/szy/entry-task/global/inner_error"
	http_server "github.com/szy/entry-task/server/http"
	"github.com/szy/entry-task/utils/response"
)

type Product struct {
}

func NewProduct() Product {
	return Product{}
}

func (_ *Product) Browse(c *gin.Context) {
	param := http_server.BrowseRequest{}
	err := c.ShouldBind(&param)
	if err != nil {
		response.ToErrorResponse(http_code.InvalidParams, c)
		return
	}
	svc := http_server.New(c.Request.Context())
	defer svc.Close()
	browseResp, err := svc.Browse(&param)
	if err != nil {
		response.ToErrorResponse(http_code.BrowseError, c)
		return
	}
	response.ToResponse(browseResp, c)
}

func (_ *Product) Detail(c *gin.Context) {
	param := http_server.DetailRequest{
		SessionId: c.GetString("sessionId"),
	}
	err := c.ShouldBind(&param)
	if err != nil {
		response.ToErrorResponse(http_code.InvalidParams, c)
		return
	}
	svc := http_server.New(c.Request.Context())
	defer svc.Close()
	detailResp, err := svc.Detail(&param)
	if err != nil {
		response.ToErrorResponse(http_code.DetailError, c)
		return
	}
	if detailResp.ErrorCode == inner_error.SessionCheckFail {
		response.ToErrorResponse(http_code.UserNotLogin, c)
		return
	}
	response.ToResponse(detailResp, c)
}
