package api

import (
	"github.com/gin-gonic/gin"
	"github.com/szy/entry-task/global/http_code"
	"github.com/szy/entry-task/global/inner_error"
	http_server "github.com/szy/entry-task/server/http"
	"github.com/szy/entry-task/utils/response"
)

type Comment struct{}

func NewComment() Comment {
	return Comment{}
}

func (_ *Comment) GetComment(c *gin.Context) {
	param := http_server.CommentRequest{
		SessionId: c.GetString("sessionId"),
	}
	err := c.ShouldBind(&param)
	if err != nil {
		response.ToErrorResponse(http_code.InvalidParams, c)
		return
	}
	svc := http_server.New(c.Request.Context())
	defer svc.Close()
	commentResp, err := svc.Comment(&param)
	if err != nil {
		response.ToErrorResponse(http_code.CommentError, c)
		return
	}
	if commentResp.ErrorCode == inner_error.SessionCheckFail {
		//fmt.Println("not log in check")
		response.ToErrorResponse(http_code.UserNotLogin, c)
		return
	}
	response.ToResponse(commentResp, c)
}

func (_ *Comment) PublishComment(c *gin.Context) {
	param := http_server.PublishRequest{
		SessionId: c.GetString("sessionId"),
	}
	err := c.ShouldBind(&param)
	if err != nil {
		response.ToErrorResponse(http_code.InvalidParams, c)
		return
	}
	svc := http_server.New(c.Request.Context())
	//defer svc.Close()
	commentResp, err := svc.Publish(&param)
	if err != nil {
		response.ToErrorResponse(http_code.CommentError, c)
		return
	}
	if commentResp.ErrorCode == inner_error.SessionCheckFail {
		//fmt.Println("not log in check")
		response.ToErrorResponse(http_code.UserNotLogin, c)
		return
	}
	response.ToResponse(commentResp, c)

}
