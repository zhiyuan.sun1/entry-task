package routers

import (
	"github.com/gin-gonic/gin"
	"github.com/szy/entry-task/routers/api"
	"github.com/szy/entry-task/utils/midware"
)

// NewRouter !!有个小问题，mysql存在offset的时候必须有limit，看看怎么优雅地解决一下
func NewRouter() *gin.Engine {
	r := gin.New()

	ping := api.NewPing()
	user := api.NewUser()
	product := api.NewProduct()
	comment := api.NewComment()

	freeGroup := r.Group("/api")
	{
		freeGroup.GET("/ping", ping.Ping)
		freeGroup.POST("/user/login", user.Login)
		freeGroup.POST("/user/register", user.Register)
		freeGroup.GET("/product/browse", product.Browse)
	}

	sessionGroup := r.Group("/api")
	sessionGroup.Use(midware.CheckSession)
	{
		sessionGroup.GET("/product/detail", product.Detail)
		sessionGroup.GET("/comment/comment", comment.GetComment)
		sessionGroup.POST("/comment/publish", comment.PublishComment)
	}

	return r
}
