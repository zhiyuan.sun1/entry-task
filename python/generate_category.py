import time
import pymysql.cursors
import random

conn = pymysql.connect(
    host='127.0.0.1',
    user='root',
    password='szy000',
    database='entry_task',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
)

cursor = conn.cursor()

# psw is "stress_test"
sql = """INSERT INTO category_relations (product_id, category) 
VALUES
({},{});
"""

print(sql)
for i in range(0, 1000000):
    try:
        tmp_sql = sql.format(i, random.randint(0,100000))
        for j in range(0,2):
            tmp_sql = sql.format(i, random.randint(0,100000))
            cursor.execute(tmp_sql)
        if not i % 10000:
            print(i)
            print(tmp_sql)
            conn.commit()
    except Exception as e:
        print(e)

# Closing the connection
conn.commit()
conn.close()

