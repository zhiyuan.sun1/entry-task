import time
import pymysql.cursors
import random

conn = pymysql.connect(
host='127.0.0.1',
user='root',
password='szy000',
database='entry_task',
charset='utf8mb4',
cursorclass=pymysql.cursors.DictCursor
)

cursor = conn.cursor()

# psw is "stress_test"
sql = """INSERT INTO comments (content, product_id, parent_id, user_id, create_time, modify_time) 
VALUES
("test comment content", {}, {}, {}, {}, {});
"""

sql2 = """INSERT INTO comments (content, product_id, user_id, create_time, modify_time) 
VALUES
("test comment content", {}, {}, {}, {});
"""

count = 1
print(sql)
for i in range(2, 1000000):
    for j in range(0,5):
        try:
            tmp_sql = sql2.format(i, 10000+random.randint(1,1000000), int(time.time()), int(time.time()))
            cursor.execute(tmp_sql)
            count = count+1
            save = count
            tmp_sql = sql.format(i, save, 10000+random.randint(1,1000000), int(time.time()), int(time.time()))
            cursor.execute(tmp_sql)
            count = count+1
            tmp_sql = sql.format(i, save, 10000+random.randint(1,1000000), int(time.time()), int(time.time()))
            cursor.execute(tmp_sql)
            count = count+1
        except Exception as e:
                print(e)
    if not i % 4000:
        print(i)
        print(tmp_sql)
        conn.commit()


# Closing the connection
conn.commit()
conn.close()

