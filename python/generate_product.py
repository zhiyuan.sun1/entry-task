import time
import pymysql.cursors

conn = pymysql.connect(
    host='127.0.0.1',
    user='root',
    password='szy000',
    database='entry_task',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
)

cursor = conn.cursor()

# psw is "stress_test"
sql = """INSERT INTO products (title, description, pic_url, create_time, modify_time) 
VALUES
("product_{}", "stress test product description", "xxxxxxxxxxxxxxxxx", {}, {});
"""

print(sql)
for i in range(1000000, 50000000):
    try:
        tmp_sql = sql.format(i, int(time.time()), int(time.time()))
        cursor.execute(tmp_sql)
        if not i % 100000:
            print(i)
            print(tmp_sql)
            conn.commit()
    except Exception as e:
        print(e)

# Closing the connection
conn.commit()
conn.close()

