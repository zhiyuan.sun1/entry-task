import time
import pymysql.cursors

conn = pymysql.connect(
    host='127.0.0.1',
    user='root',
    password='szy000',
    database='entry_task',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
)

cursor = conn.cursor()

# psw is "stress_test"
sql = """INSERT INTO users (log_id, password, nickname, create_time, modify_time) 
VALUES
("stress_test_{}", "1262c3bd8ce6df3c3379286c4bf5933f", "stress_test_nickname", {}, {});
"""

print(sql)
for i in range(10004, 1010000):
    try:
        tmp_sql = sql.format(i, int(time.time()), int(time.time()))
        cursor.execute(tmp_sql)
        if not i % 10000:
            print(i)
            print(tmp_sql)
            conn.commit()
    except Exception as e:
        print(e)

# Closing the connection
conn.commit()
conn.close()

