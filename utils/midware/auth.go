package midware

import (
	"github.com/gin-gonic/gin"
	"github.com/szy/entry-task/global/http_code"
	"github.com/szy/entry-task/utils/response"
)

func CheckSession(c *gin.Context) {
	sessionId, err := c.Cookie("session_id")
	if err != nil {
		response.ToAbortErrorResponse(http_code.UserNotLogin, c)
		return
	} else {
		c.Set("sessionId", sessionId)
		c.Next()
	}
}
