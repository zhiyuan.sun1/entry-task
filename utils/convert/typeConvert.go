package convert

import (
	"github.com/szy/entry-task/utils/log"
	"strconv"
)

func SliceInt64ToStr(s []int64) []string {
	var rt = make([]string, len(s))
	for i, n := range s {
		rt[i] = strconv.FormatInt(n, 10)
	}
	return rt
}

func SliceStrToInt64(s []string) []int64 {
	var rt = make([]int64, len(s))
	var err error
	for i, str := range s {
		rt[i], err = strconv.ParseInt(str, 10, 64)
		if err != nil {
			log.Logln(err)
		}
	}
	return rt
}
