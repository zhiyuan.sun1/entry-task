package response

import (
	"github.com/gin-gonic/gin"
	"github.com/szy/entry-task/global/http_code"
	"net/http"
)

func ToResponse(data interface{}, ctx *gin.Context) {
	data = gin.H{"err_code": 0, "msg": "success", "data": data}
	ctx.JSON(http.StatusOK, data)
}

func ToErrorResponse(err http_code.Error, ctx *gin.Context) {
	response := gin.H{"err_code": err.GetCode(), "msg": err.GetMessage()}
	ctx.JSON(err.GetHttpStatus(), response)
}

func ToAbortErrorResponse(err http_code.Error, ctx *gin.Context) {
	response := gin.H{"err_code": err.GetCode(), "msg": err.GetMessage()}
	ctx.AbortWithStatusJSON(err.GetHttpStatus(), response)
}
