package recover

import (
	"context"
	"github.com/szy/entry-task/utils/log"
	"google.golang.org/grpc"
)

func Recovery(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	defer func() {
		if e := recover(); e != nil {
			log.Logln(e)
		}
	}()
	return handler(ctx, req)
}
