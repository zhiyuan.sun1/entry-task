package psw

import (
	"crypto/md5"
	"encoding/hex"
	"github.com/szy/entry-task/global"
)

// HashPassword: Added salt and return a hashed string
func HashPassword(input string) string {
	return HashWithMD5(global.HashSalt + input)
}

// HashWithMD5: Return a MD5'd string
func HashWithMD5(input string) string {
	hash := md5.Sum([]byte(input))
	return hex.EncodeToString(hash[:])
}

func CheckPsw(input string, psw string) bool {
	return HashPassword(psw) == input
}
