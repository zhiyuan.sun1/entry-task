package dao

import (
	"github.com/szy/entry-task/models"
	"gorm.io/gorm"
)

func (dao *Dao) GetProductIdList(limit int64, title string, category int32) ([]int64, error) {
	var whereMap map[string]interface{}
	whereMap = nil
	if title != "" {
		whereMap = make(map[string]interface{})
		whereMap["title"] = title
	}
	idList, db := getProductIdList(dao.engin, limit, whereMap, category)
	return idList, db.Error
}

func (dao *Dao) GetProductList(list []int64) ([]models.ProductInfo, error) {
	infoList, db := getProductListInIdList(dao.engin, list)
	return infoList, db.Error
}

// GetProductList_no_use no use
func (dao *Dao) GetProductList_no_use(cursor int64, limit int64) ([]models.ProductInfo, error) {
	productInfoList, db := getProductList(dao.engin, cursor, limit, nil, 0)
	return productInfoList, db.Error
}

// GetProductListByTitle_no_use no use
func (dao *Dao) GetProductListByTitle_no_use(title string, cursor int64, limit int64) ([]models.ProductInfo, error) {
	whereMap := make(map[string]interface{})
	whereMap["title"] = title
	productInfoList, db := getProductList(dao.engin, cursor, limit, whereMap, 0)
	return productInfoList, db.Error
}

// GetProductListByCategory_no_use no use
func (dao *Dao) GetProductListByCategory_no_use(category int32, cursor int64, limit int64) ([]models.ProductInfo, error) {
	productInfoList, db := getProductList(dao.engin, cursor, limit, nil, category)
	return productInfoList, db.Error
}

func (dao *Dao) GetProductDetail(id int64) (*models.ProductDetail, error) {
	productDetail, db := getProductDetail(dao.engin, id)
	if db.Error != nil {
		return nil, db.Error
	}
	categories, db := getProductCategories(dao.engin, id)
	if db.Error == nil {
		productDetail.Categories = categories
	}
	return productDetail, db.Error
}

func getProductIdList_toDelete(db *gorm.DB) ([]int64, *gorm.DB) {
	var idList []int64
	db = db.Table("products").Select("id").Find(&idList)
	return idList, db
}

func getProductIdList(db *gorm.DB, limit int64, whereMap map[string]interface{}, category int32) ([]int64, *gorm.DB) {
	var idList []int64
	if whereMap == nil {
		whereMap = make(map[string]interface{})
	}
	db = db.Table("products").Select("id").Limit(int(limit))
	if category != 0 {
		db = db.Joins("inner join category_relations on category_relations.product_id=products.id")
		whereMap["category"] = category
	}
	if len(whereMap) != 0 {
		db.Where(whereMap)
	}
	db = db.Find(&idList)
	return idList, db
}

func getProductListInIdList(db *gorm.DB, idList []int64) ([]models.ProductInfo, *gorm.DB) {
	var productInfoList []models.ProductInfo
	//db = db.Table("products").Select("id, title, pic_url").Where("id in ", idList).Find(&productInfoList)
	db = db.Raw("select id,title,pic_url from products where id in ?", idList).Find(&productInfoList)
	return productInfoList, db
}

func getProductList(db *gorm.DB, cursor int64, limit int64, whereMap map[string]interface{}, category int32) ([]models.ProductInfo, *gorm.DB) {
	var productInfoList []models.ProductInfo
	if whereMap == nil {
		whereMap = make(map[string]interface{})
	}
	db = db.Table("products").Select("id, title, pic_url").Offset(int(cursor)).Limit(int(limit))
	if category != 0 {
		db = db.Joins("inner join category_relations on category_relations.product_id=products.id")
		whereMap["category"] = category
	}
	if len(whereMap) != 0 {
		db.Where(whereMap)
	}
	db = db.Find(&productInfoList)
	return productInfoList, db
}

func getProductDetail(db *gorm.DB, id int64) (*models.ProductDetail, *gorm.DB) {
	var productDetail models.ProductDetail
	db = db.Table("products").Where("id = ?", id).First(&productDetail)
	return &productDetail, db
}

func getProductCategories(db *gorm.DB, id int64) ([]int32, *gorm.DB) {
	var categories []int32
	db = db.Table("category_relations").Select("category").Where("product_id = ?", id).Find(&categories)
	return categories, db
}
