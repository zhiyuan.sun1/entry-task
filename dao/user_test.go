package dao

import (
	"database/sql"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"os"
	"testing"
	"time"
)

var (
	count = 0
)

func BenchmarkDao_InsertUser(b *testing.B) {
	dsn := "root:szy000@tcp(127.0.0.1:3306)/entry_task?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	connPool, err := sql.Open("mysql", dsn)
	connPool.SetMaxOpenConns(1)
	//connPool.SetMaxIdleConns(1)
	//connPool.SetConnMaxIdleTime(time.Hour)
	connPool.SetConnMaxLifetime(time.Hour)
	if err != nil {
		fmt.Println(err)
	}
	engin, err := gorm.Open(mysql.New(mysql.Config{Conn: connPool}))

	//db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln("err: ", err)
		os.Exit(1)
	}
	b.SetParallelism(128)

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			count++
			u, db := getUserByLogId(engin, "stress_test_19999")
			fmt.Println(u, db.Error)
		}

	})
}
