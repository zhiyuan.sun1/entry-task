package dao

import (
	"github.com/szy/entry-task/global/resource"
	"gorm.io/gorm"
)

type Dao struct {
	engin *gorm.DB
}

func New() *Dao {
	return &Dao{engin: resource.DBEngine}
}
