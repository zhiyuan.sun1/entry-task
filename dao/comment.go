package dao

import (
	"github.com/szy/entry-task/models"
	"gorm.io/gorm"
)

func (dao *Dao) InsertComment(comment *models.PublishComment) error {
	if comment.ParentId == 0 {
		return insertComment(dao.engin, comment).Error
	} else {
		return insertSubComment(dao.engin, comment).Error
	}
}

func (dao *Dao) GetComments(productId int64) ([]models.Comment, error) {
	commentList, db := getComments(dao.engin, productId)
	return commentList, db.Error
}

func (dao *Dao) GetCommentList(idList []int64) ([]models.Comment, error) {
	commentList, db := getCommentInIdList(dao.engin, idList)
	return commentList, db.Error
}

func (dao *Dao) GetCommentIdList(productId int64, cursor int64, limit int64) ([]int64, error) {
	//whereMap := make(map[string]interface{})
	//whereMap["product_id"] = productId
	//whereMap["parent_id"] = cache2.NilCommentId
	idList, db := getCommentIdList(dao.engin, productId, cursor, limit)
	return idList, db.Error
}

func (dao *Dao) GetSubCommentIdFromIdList(idList []int64) ([]models.Pair, error) {
	pairList, db := getSubCommentIdInIdList(dao.engin, idList)
	return pairList, db.Error
}

func getComments(db *gorm.DB, productId int64) ([]models.Comment, *gorm.DB) {
	var commentList []models.Comment
	db = db.Table("comments").Where("product_id=?", productId).Find(&commentList)
	return commentList, db
}

func getCommentIdList(db *gorm.DB, productId int64, cursor int64, limit int64) ([]int64, *gorm.DB) {
	var idList []int64
	//db.Raw("select id from comments where product_id=? and parent_id is null offset ?,?", )
	db = db.Table("comments").Select("id").Where("product_id=? and parent_id is null", productId).Offset(int(cursor)).Limit(int(limit)).Find(&idList)
	return idList, db
}

func getSubCommentIdInIdList(db *gorm.DB, idList []int64) ([]models.Pair, *gorm.DB) {
	var subList []models.Pair
	db = db.Raw("select id, parent_id from comments where parent_id in ?", idList).Find(&subList)
	return subList, db
}

func getCommentInIdList(db *gorm.DB, idList []int64) ([]models.Comment, *gorm.DB) {
	var commentList []models.Comment
	db = db.Raw("select * from comments where id in ?", idList).Find(&commentList)
	return commentList, db
}

func insertComment(db *gorm.DB, comment *models.PublishComment) *gorm.DB {
	return db.Exec("insert into comments (product_id, user_id, content, create_time, modify_time) values(?,?,?,?,?)",
		comment.ProductId, comment.UserId, comment.Content, comment.CreateTime, comment.ModifyTime)
}

func insertSubComment(db *gorm.DB, comment *models.PublishComment) *gorm.DB {
	return db.Table("comments").Create(comment)
}
