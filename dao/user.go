package dao

import (
	"github.com/szy/entry-task/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func (dao *Dao) GetUserByLogId(logId string) (models.User, error) {
	userLogInfo, db := getUserByLogId(dao.engin, logId)
	return userLogInfo, db.Error
}

func (dao *Dao) InsertUser(userLogInfo models.User) (int64, error) {
	db := insertUser(dao.engin, userLogInfo)
	return db.RowsAffected, db.Error
}

func getUserByLogId(db *gorm.DB, logId string) (models.User, *gorm.DB) {
	var userLogInfo models.User
	return userLogInfo, db.Where("log_id = ?", logId).Take(&userLogInfo)
}

func insertUser(db *gorm.DB, userLogInfo models.User) *gorm.DB {
	return db.Clauses(clause.Insert{Modifier: "IGNORE"}).Create(&userLogInfo)
}
